# TD GIT
## git log --graph --oneline --all
```
*   9dac961 (HEAD -> feature-add-readme, tag: v1.1.0, origin/main, main) Merge branch 'develop'
|\
| *   69da7cd (origin/develop, develop) Merge branch 'feature-x' into develop
| |\
| | * d805d33 (origin/feature-x, feature-x) [fix] correction du du body
| | * 246c65d [fix] correction du titre
| |/
| * 3c5a285 [feat] mise à jour texte dans le body index.html en gras
|/
* e342027 (tag: v1.0.0) [feat] modification du titre
* f7001a2 initial commit
```

## Commandes executées :

**Création du fichier index.html et push du commit initial**
```
$ mkdir training-git
$ vi index.html
$ git add index.html
$ git commit -m "initial commit"
$ git remote add origin git@gitlab.com:airdee/td-training-git.git
$ git push --set-upstream origin --all
```


**FEAT 1 : Modification du fichier index.html, ajout d'un tag 1.0.0 et push vers origin**
```
$ vi index.html
$ git add index.html
$ git commit -m "[feat] modification du titre"
$ git tag v1.0.0
$ git push 
$ git push --tags
```


**Création de la branche develop**
```
$ git checkout -b develop
```

**FEAT 2 : Modification du texte dans le body en gras et push vers origin**
```
$ vi index.html
$ git add index.html
$ git commit -m "[feat] mise à jour texte dans le body index.html en gras"
$ git push origin develop
```


**Création de la branche feature-x**
```
$ git checkout -b feature-x
```

**FIX 1 : Correction du titre dans la branche feature-x et push vers origin**
```
$ vi index.html
$ git add index.html
$ git commit -m "[fix] correction du titre"
$ git push origin feature-x
```

**FIX 2 : Correction du body dans le branche feature-x et push vers origin**
```
$ vi index.html
$ git add index.html
$ git commit -m "[fix] correction du body"
$ git push origin feature-x
```


**Merge de la branch feature-x dans la branch develop (sans fast-forward)**
```
$ git checkout develop
$ git merge --no-ff feature-x
$ git push origin develop
```


**Merge de la branch develop dans la branch main (sans fast-forward)**
```
$ git checkout main
$ git merge --no-ff develop
$ git push
```


**Ajout et push d'un tag v1.1.0**
```
$ git tag v1.1.0
$ git push origin --tags
```
